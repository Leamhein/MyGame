/*import '../../components/modal-dialog/modal.css';

import '../../css/index.css';
*/
import {
    resCache,
    resources
} from '../../script/engine/resources';

import {
    loop
} from '../../script/engine/loop';

import {
    update
} from '../../script/engine/update';

import {
    draw
} from '../../script/engine/draw';

import {
    Sprite
} from '../../script/classes/sprite';

import {
    Player
} from '../../script/classes/Player';

import {
    duelStart
} from '../../script/engine/duel';

import {
    showLeaderboard
} from '../../script/engine/leaderboard';
// массив спрайтов
let SpriteArr = [
    './image/landing-background.png',
    './image/landing-middleground.png',
    './image/blocks.png',
    './image/wizard-standing.png',
    './image/the-tower-red.png',
    './image/monster-armor-blue.png',
    './image/monster-armor-green.png',
    './image/monster-armor-yellow.png',
    './image/monster-armor-red.png',
    './image/monster-head-1.png',
    './image/monster-head-2.png',
    './image/monster-head-3.png',
    './image/monster-head-4.png',
    './image/sword.png',
    './image/swordWood.png',
    './image/axe.png',
    './image/axe2.png',
    './image/axeDouble.png',
    './image/axeDouble2.png',
    './image/hammer.png',
    './image/wand.png',
    './image/firebolt.png',
    './image/thunderbolt.png',
    './image/icebolt.png',
    './image/Cast_127х69.png'
];
//массив с адресами данных в JSON формате
let JsonArr = [
    'https://raw.githubusercontent.com/Leamhein/Json/master/Dictionary.json',
    'https://raw.githubusercontent.com/Leamhein/Json/master/monsterNames.json',
    'https://raw.githubusercontent.com/Leamhein/Json/master/history.json',
    'https://raw.githubusercontent.com/Leamhein/Json/master/colors-dictionary.json',
    'https://raw.githubusercontent.com/Leamhein/Json/master/animals.json',
    'https://raw.githubusercontent.com/Leamhein/Json/master/idioms-dictionary.json'
];
export let canvas = document.getElementById('canvas');
export let ctx = canvas.getContext('2d');
export let tower;
export let character;
export let renderObj = {};

resources(SpriteArr, JsonArr);
//запуск цикла игры
export function start() {
    //объявление сущности игрока
    character = new Sprite(['character'], './image/wizard-standing.png', [0, 0], [70, 70], 1, 10, 3, [0, (canvas.height - 70)], [0, 0], false);
    let name = `playerName`;

    let player = new Player(100, [0, (canvas.height - 70)], 50, name, 0);
    //создание спрайта башни (Landing page)
    tower = new Sprite(['tower'], './image/the-tower-red.png', [-127, 0], [127, 250], 0.6, 0, 1, [((canvas.width / 2) + 70), (canvas.height - 150)], [0, 0], false);
    let leaderboardBtn = document.querySelector('#leaderboard-btn');
    let playBtn = document.getElementById('play-btn');
    //отрисовка игрока и башни
    renderObj['character'] = character;
    renderObj['tower'] = tower;

    leaderboardBtn.addEventListener('click', showLeaderboard);
    playBtn.addEventListener('click', gameStart);
    loop.start();
    //Присваивание имени игроку, сокрытие landing-page
    function gameStart() {
        let landingPage = document.getElementById('landingPg');
        landingPage.style.display = 'none';
        player.name = prompt('Your name, please');
        let footer = document.querySelector('footer');
        let landingPg = document.getElementById('landingPg');
        landingPg.classList.add('hide');
        footer.innerHTML = "";
        footer.style.borderTop = "none";
        footer.style.backgroundImage = "url(./image/blocks.png)";
        footer.style.backgroundRepeat = "repeat-x";
        footer.style.backgroundSize = "contain";

        let header = document.querySelector("header");
        header.innerHTML = `<label class = "player-name">${player.name}</label><div class = "sidebar"><label class = "player-hp" >0</label></div><button class = 'leaderboardBtn'>Leaderboard</button><button class="restartBtn">Reastart</button><label class = "monster-name">Someme</label><div class = "sidebar"><label class = "monster-hp">0</label></div>`;

        let leaderboardBtn = document.querySelector('.leaderboardBtn');
        leaderboardBtn.addEventListener('click', showLeaderboard);

        let restartBtn = document.querySelector('.restartBtn');
        restartBtn.addEventListener('click', () => {
            const modal = document.getElementById('modal-dialog');
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            duelStart(1, renderObj, player);
        });
        duelStart(1, renderObj, player);
    };

}
