let resourceNum;
let loadingScr = require("html-loader!./loading-screen.html");
document.querySelector('footer').insertAdjacentHTML('afterend', loadingScr);
loadingScr = document.getElementById('loading-screen');
const counter = document.getElementById('counter');

export function showLoadingScr(boolian, iterationNum) {
    if (!resourceNum) {
        resourceNum = iterationNum;
    }

    let currentPersent = Math.floor((resourceNum - iterationNum) / (resourceNum / 100));

    if (!iterationNum) {
        currentPersent = 100;
    }

    if (boolian) {
        counter.innerHTML = currentPersent + '%';
        loadingScr.style.display = "flex";
    } else {
        loadingScr.parentNode.removeChild(loadingScr);
    }
}
