import {
    resCache
} from '../../script/engine/resources';

import {
    loop,
    started,
    running
} from '../../script/engine/loop';

Array.prototype.shuffle = function () {
    for (let i = this.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
};

export function historyTask(callback) {
    const canvas = document.getElementById('canvas');
    let modal = require("html-loader!../../components/modal-dialog/modal.html");
    canvas.insertAdjacentHTML('afterend', modal);

    const historyDates = resCache['JSON']['https://raw.githubusercontent.com/Leamhein/Json/master/history.json'];

    loop.stop();

    const rand = Object.keys(historyDates)[Math.floor(Math.random() * Object.keys(historyDates).length)];
    const answer = historyDates[rand][0];
    let arr = historyDates[rand].shuffle();

    modal = document.getElementById('modal-dialog');
    let description = document.querySelector('.description');
    description.innerHTML = `<h2>${rand}</h2>`;
    description.style.display = 'block';
    let btnHolder = document.querySelector('.buttonHolder');
    btnHolder.style.display = 'flex';
    let btnArr = btnHolder.children;
    for (let i = 0; i < btnArr.length; i++) {
        btnArr[i].innerHTML = `${arr[i]}`;
    }

    modal.addEventListener('click', function () {
        const target = event.target;
        const select = +event.target.innerHTML;



        if (target.tagName != 'BUTTON') return;

        if (target.innerHTML == answer) {
            callback(true);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        } else {
            callback(false);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        }
    })
}
