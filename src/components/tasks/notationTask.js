import {
    loop,
    started,
    running
} from '../../script/engine/loop';

export function notationTask(callback) {
    const canvas = document.getElementById('canvas');
    let modal = require("html-loader!../../components/modal-dialog/modal.html");
    canvas.insertAdjacentHTML('afterend', modal);

    loop.stop();

    const randNum = Math.round(Math.random() * 50);
    const notation = Math.round(Math.random() * 34) + 2;
    const answer = randNum.toString(notation);

    modal = document.getElementById('modal-dialog');
    let description = document.querySelector('.description');
    description.innerHTML = `<h2>convert number from decimal to ${notation} number system </h2><p>${randNum}</p>`;
    description.style.display = 'block';
    const input = document.getElementById('input');
    input.style.display = 'block';
    input.focus();
    const submitButton = document.getElementById('submitB');
    submitButton.style.display = 'block';

    submitButton.addEventListener('click', () => {
        check(callback)
    });

    modal.addEventListener('keypress', (e) => {
        if (e.key == 'Enter') {
            check(callback);
        }
    })

    function check(callback) {
        let inputText = input.value.toLowerCase();

        if (answer === inputText) {
            callback(true);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        } else {
            callback(false);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        }
    }
}
