import {
    loop,
    started,
    running
} from '../../script/engine/loop';

export function triangleTask(callback) {
    const canvas = document.getElementById('canvas');
    let modal = require("html-loader!../../components/modal-dialog/modal.html");
    canvas.insertAdjacentHTML('afterend', modal);

    loop.stop();

    let triangle = triangleGenerator();

    modal = document.getElementById('modal-dialog');
    let description = document.querySelector('.description');
    description.innerHTML = '<h2>specify the dimension of the third angle of the triangle</h2>';
    description.style.display = 'block';
    const svg = document.querySelector('svg');

    svg.innerHTML = `<polygon points="${triangle.coordinats[0][0]},${triangle.coordinats[0][1]} ${triangle.coordinats[1][0]},${triangle.coordinats[1][1]} ${triangle.coordinats[2][0]},${triangle.coordinats[2][1]}"
                 fill="white" stroke="red" stroke-width="2"/>
            <text x="${triangle.coordinats[0][0]-20}" y="${triangle.coordinats[0][1]}">${triangle.angles[0]}</text>
            <text x="${triangle.coordinats[1][0]+5}" y="${triangle.coordinats[1][1]}">${triangle.angles[1]}</text>
            <text x="${triangle.coordinats[2][0]+8}" y="${triangle.coordinats[2][1]}">?</text>`;
    svg.style.display = 'block';
    const input = document.getElementById('input');
    input.style.display = 'block';
    input.value = '';
    input.focus();
    const submitButton = document.getElementById('submitB');
    submitButton.style.display = 'block';

    submitButton.addEventListener('click', () => {
        let inputText = +input.value;

        if (triangle.angles[2] === inputText) {
            callback(true);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        } else {
            callback(false);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        }
    })
}

function triangleGenerator() {
    //задание трех углов треугольника
    const firstAngle = Math.round(Math.random() * (89 - 20)) + 20;
    const secondAngle = Math.round((Math.random() * (45 - 20)) + 20);
    const answer = 180 - firstAngle - secondAngle;
    //расчет длинн сторон треугольника
    const AB = 100;
    const BC = Math.floor(AB * (Math.sin(firstAngle / 180 * Math.PI) / Math.sin(answer / 180 * Math.PI)));
    const AC = Math.floor(AB * (Math.sin(secondAngle / 180 * Math.PI) / Math.sin(answer / 180 * Math.PI)));
    //рассчет координат вершин треугольника
    var A = [50, 90]; // начальные координаты
    var B = [50 + AB, 90]; // координаты второй вершины
    var C = [];

    C[0] = (AB * AB + AC * AC - BC * BC) / (2 * AB);
    C[1] = Math.sqrt(AC * AC - C[0] * C[0]);
    // координаты третьей вершины
    C[0] = 50 + C[0];
    C[1] = 90 - C[1];

    return {
        coordinats: [A, B, C],
        angles: [firstAngle, secondAngle, answer]
    };
}
