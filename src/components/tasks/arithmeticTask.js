import {
    resCache
} from '../../script/engine/resources';

import {
    loop,
    started,
    running
} from '../../script/engine/loop';

export function arithmeticTask(callback) {
    const canvas = document.getElementById('canvas');
    let modal = require("html-loader!../../components/modal-dialog/modal.html");
    canvas.insertAdjacentHTML('afterend', modal);

    loop.stop();

    modal = document.getElementById('modal-dialog');
    let description = document.querySelector('.description');
    description.innerHTML = '<h2>Solve it</h2>';
    description.insertAdjacentHTML('beforeend', "<p class = 'ex'></p>");
    description.style.display = 'block';
    const input = document.getElementById('input');
    input.style.display = 'block';
    input.focus();
    const submitButton = document.getElementById('submitB');
    submitButton.style.display = 'block';

    const exercise = document.querySelector('.ex');

    let opArr = ['+', '-', '*', '/'];
    let op = opArr[Math.floor(Math.random() * (opArr.length))];
    let num1 = Math.floor(Math.random() * (199)) + 1;
    let num2 = Math.floor(Math.random() * (199)) + 1;
    let ex;

    if (op == '/' && num1 < num2) {
        ex = `${num2}${op}${num1}`;

    } else {
        ex = `${num1}${op}${num2}`;
    }
    exercise.textContent = ex;
    let solution = Math.round(eval(ex));

    submitButton.addEventListener('click', () => {
        check(callback)
    });

    modal.addEventListener('keypress', (e) => {
        if (e.key == 'Enter') {
            check(callback);
        }
    })

    function check(callback) {
        let inputSolution = parseInt(input.value);

        if (inputSolution == solution) {
            callback(true);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        } else {
            callback(false);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        }
    }
}
