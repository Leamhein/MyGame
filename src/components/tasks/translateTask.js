import {
    resCache
} from '../../script/engine/resources';

import {
    loop,
    started,
    running
} from '../../script/engine/loop';

export function translateTask(callback) {
    const canvas = document.getElementById('canvas');
    let modal = require("html-loader!../../components/modal-dialog/modal.html");
    canvas.insertAdjacentHTML('afterend', modal);

    const dictionary = resCache['JSON']['https://raw.githubusercontent.com/Leamhein/Json/master/Dictionary.json'];

    loop.stop();

    const rand = Object.keys(dictionary)[Math.floor(Math.random() * Object.keys(dictionary).length)];

    modal = document.getElementById('modal-dialog');
    let description = document.querySelector('.description');
    description.innerHTML = `<h2>Translate this word into Russian:</h2><p>${rand}</p>`;
    description.style.display = 'block';
    const input = document.getElementById('input');
    input.style.display = 'block';
    input.focus();
    const submitButton = document.getElementById('submitB');
    submitButton.style.display = 'block';

    submitButton.addEventListener('click', () => {
        check(callback)
    });

    modal.addEventListener('keypress', (e) => {
        if (e.key == 'Enter') {
            check(callback);
        }
    })

    function check(callback) {
        let inputText = input.value.toLowerCase();

        if (dictionary[rand].some((i) => {
                return i === inputText
            })) {
            callback(true);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        } else {
            callback(false);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        }
    }
}
