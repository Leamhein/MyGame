import {
    resCache
} from '../../script/engine/resources';

import {
    loop,
    started,
    running
} from '../../script/engine/loop';

export function idiomTask(callback) {
    const canvas = document.getElementById('canvas');
    let modal = require("html-loader!../../components/modal-dialog/modal.html");
    canvas.insertAdjacentHTML('afterend', modal);

    const dictionary = resCache['JSON']['https://raw.githubusercontent.com/Leamhein/Json/master/idioms-dictionary.json'];

    loop.stop();

    const rand = Object.keys(dictionary)[Math.floor(Math.random() * Object.keys(dictionary).length)];

    let arr = dictionary[rand];

    modal = document.getElementById('modal-dialog');
    let description = document.querySelector('.description');
    description.innerHTML = '<h2>Correct this idiom</h2>';
    description.style.display = 'block';
    const input = document.querySelector('#sortable');
    input.style.display = 'flex';
    const submitButton = document.getElementById('submitB');
    submitButton.style.display = 'block';
    submitButton.focus();

    $(function () {
        $("#sortable").sortable({
            axis: "x"
        });
        $("#sortable").disableSelection();
    });


    function compareRandom(a, b) {
        return Math.random() - 0.5;
    }

    arr.sort(compareRandom);


    for (let i = 0; i < arr.length; i++) {
        let li = document.createElement('li');
        li.innerHTML = arr[i];
        input.appendChild(li);
    }

    submitButton.addEventListener('click', () => {
        check(callback)
    });

    modal.addEventListener('keypress', (e) => {
        if (e.key == 'Enter') {
            check(callback);
        }
    });

    modal.addEventListener('keypress', (e) => {
        if (/\d/.test(e.key) && e.key != '1' && e.key != '0' && parseInt(e.key) <= arr.length) {
            let i = parseInt(e.key) - 1;
            let temp = input.children[i].cloneNode(true);
            temp = input.replaceChild(temp, input.children[i - 1]);
            temp = input.replaceChild(temp, input.children[i]);
        }
    });

    function check() {
        let inputText = '';

        for (let i = 0; i < arr.length; i++) {
            inputText += input.children[i].innerHTML;
        }

        if (rand == inputText) {
            callback(true);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        } else {
            callback(false);
            if (modal) {
                modal.parentNode.removeChild(modal);
            }
            if (!started && !running) {
                loop.start();
            }
        }
    };
}
