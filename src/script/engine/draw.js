import {
    ctx,
    renderObj
} from '../../screens/home/index';

export function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height); // очистили прошлый кадр
    for (let obj in renderObj) { // рендерим объекты
        renderObj[obj].render(ctx);
    }
}
