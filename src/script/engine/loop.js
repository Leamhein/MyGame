import {
    draw
} from './draw';

import {
    character,
    tower,
    ctx
} from '../../screens/home/index';

import {
    update
} from './update';

export let running = false;
export let started = false;
let startTime;
let delta = 0;
let frame;

export function loop() {
    if (!startTime) {
        startTime = Date.now();
    }
    const time = Date.now();
    delta = (time - startTime) / 100;
    if (!delta) {
        delta = 1;
    }
    update(delta);
    draw();
    startTime = time;
    frame = requestAnimationFrame(loop);
}

loop.stop = () => {
    running = false;
    started = false;
    cancelAnimationFrame(frame);
}

loop.start = () => {
    if (!started) {
        started = true;
        running = true;
        frame = requestAnimationFrame(loop);
    }
}
