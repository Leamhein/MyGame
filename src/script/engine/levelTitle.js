import {
	duelObj
} from './duel';


let opacity = 100;
let dataTake = false;
let monsterName;
let level;


export function levelTitle() {

	let main = document.getElementById('main');
	let article = document.createElement('article');
	article.className = 'levelTitle';
	main.insertBefore(article, main.children[0]);

	if (!dataTake) {
		monsterName = duelObj.monster.name;
		level = duelObj.level;
		dataTake = true;
	}

	if (duelObj.level > 1) {
		article.innerHTML = `<p>${monsterName} is defeated! Going to the level ${level}!</p>`;
	} else {
		article.innerHTML = `<p>current level is ${level} Let's battle begin!</p>`;
	};
	let animation = requestAnimationFrame(hide);

	function hide() {

		article.style.opacity = "" + opacity / 100;
		opacity--;
		animation = requestAnimationFrame(hide);
		if (opacity <= 0) {
			article.remove();
			opacity = 100;
			cancelAnimationFrame(animation);
			dataTake = false;
		}
		return;
	}
}
