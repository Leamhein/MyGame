import {
    renderObj
} from '../../screens/home/index';

export function update(delta) {
    if (renderObj['bolt']) {
        if (renderObj['bolt']['startPosOnCanv'][0] + renderObj['bolt']['size'][0] >= renderObj['monsterBody']['startPosOnCanv'][0] + 5) {
            renderObj['bolt'].hit();
            delete renderObj['bolt'];
        }
    }
    if (renderObj['monsterWeapon']) {
        if ((renderObj['monsterWeapon']['startPosOnCanv'][0] - renderObj['monsterWeapon']['size'][0]) <= (renderObj['character']['startPosOnCanv'][0]) - 35) {

            renderObj['monsterWeapon'].speed = [0, 0];
            renderObj['monsterWeapon'].hit();
            renderObj['monsterWeapon'].startPosOnCanv = [(canvas.width - 85), (canvas.height - 100)];
        }
    }
    for (let key in renderObj) {
        if (!renderObj[key]['speed'][0] || !renderObj[key]['speed'][1]) {
            renderObj[key]['startPosOnCanv'][0] += renderObj[key]['speed'][0] / delta;
            renderObj[key]['startPosOnCanv'][1] += renderObj[key]['speed'][1] / delta;
        }
    }
};
