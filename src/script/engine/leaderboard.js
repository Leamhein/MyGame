export function updateLeaderboard(obj){
    firebase.database().ref('/users/').update(obj).then(() => {
        showLeaderboard();
        alert('refresh the page to restart');
    }, () => {
        console.log('# rejected');
    })
};
export function showLeaderboard() {

    let database = firebase.database().ref('/users/').once('value').then(function (snapshot) {
        return snapshot.val();
    }).then((data) => {

        let board = document.createElement('div');
        let closeBtn = document.createElement('div');
        closeBtn.innerHTML = `<i class="fas fa-times"></i>`;
        closeBtn.classList.add('close-leaderboard-btn')
        board.classList.add('modal');
        board.classList.add('board-container');

        let table = document.createElement("table");
        table.classList.add('board-table');
        let tbody = document.createElement('tbody');
        tbody.innerHTML = "<thead><td>Name</td><td>Score</td></thead>";
        table.appendChild(tbody);

        let keysSorted = Object.keys(data).sort(function (a, b) {
            return data[a] - data[b]
        })

        keysSorted.reverse().forEach((name) => {
            let row = document.createElement('tr');
            row.innerHTML = `<td>${name}</td><td>${data[name]}</td>`
            tbody.appendChild(row);
        });

        board.appendChild(table);
        board.appendChild(closeBtn);
        document.body.appendChild(board);

        closeBtn.addEventListener('click', () => {
            document.body.removeChild(board);
        })
    });
};