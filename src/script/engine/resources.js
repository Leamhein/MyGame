import {
    start
} from '../../screens/home/index';

import {
    showLoadingScr
} from '../../components/loading-screen/loading-screen'

export let resCache = {
    'images': {},
    'JSON': {}
};

export function resources(SpriteArr, JsonArr) {

    let resourcesCount = SpriteArr.length + JsonArr.length;

    showLoadingScr(true, resourcesCount);

    SpriteArr.forEach((url) => {
        load(url);
    });

    JsonArr.forEach((url) => {
        readJsonFile(url);
    })

    function load(url) {
        let img = new Image();
        img.src = url;
        img.onload = () => {
            resCache['images'][url] = img;
            isReady();
        }
    }

    function readJsonFile(url) {
        const rawFile = new XMLHttpRequest();
        rawFile.overrideMimeType("application/json");
        rawFile.open("GET", url, true);
        rawFile.onreadystatechange = () => {
            if (rawFile.readyState === 4 && rawFile.status == "200") {
                resCache['JSON'][url] = JSON.parse(rawFile.responseText);
                isReady();
            }
        }
        rawFile.send(null);
    }

    function isReady() {
        resourcesCount--;
        if (!resourcesCount) {
            showLoadingScr(false, resourcesCount);
            start();
            return
        }
        showLoadingScr(true, resourcesCount);
    }
}

resources.get = (url) => resCache['images'][url];
