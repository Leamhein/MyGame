import {
    Monster
} from '../classes/Monster';
import {
    Sprite
} from '../classes/sprite';
import {
    listeningTask
} from '../../components/tasks/listeningTask';

import {
    translateTask
} from '../../components/tasks/translateTask';

import {
    arithmeticTask
} from '../../components/tasks/arithmeticTask';

import {
    notationTask
} from '../../components/tasks/notationTask';

import {
    historyTask
} from '../../components/tasks/historyTask';

import {
    triangleTask
} from '../../components/tasks/triangleTask';

import {
    dragndropTask
} from '../../components/tasks/dragndropTask';

import {
    colorTask
} from '../../components/tasks/colorsTask';

import {
    animalsTask
} from '../../components/tasks/animalTask';

import {
    idiomTask
} from '../../components/tasks/idiomTask';

import {
    resCache
} from './resources';

import {
    levelTitle
} from './levelTitle';

import {
    renderObj
} from '../../screens/home/index';

import {
    updateLeaderboard
} from './leaderboard';

let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
export let duelObj = {};

export function duelStart(level, renderObj, player) {
    //реген хп
    player.health = 100;

    //позиция монстра
    const monsterBodyPosX = (canvas.width - 45);
    const monsterBodyPosY = (canvas.height - 100);

    //очищаем старого монстра и башню
    clearBattlefield(renderObj);
    //инициализируем монстра и отрисовываем имя
    let monster = new Monster(100, [monsterBodyPosX, monsterBodyPosY], (level * 10));
    document.querySelector('.monster-name').textContent = monster.generateName();

    //рисуем рандомного монстра
    renderObj.monsterWeapon = monster.generateWeapon(monsterBodyPosX, monsterBodyPosY);
    renderObj.monsterBody = monster.generateBody(monsterBodyPosX, monsterBodyPosY);
    renderObj.monsterHead = monster.generateHead(monsterBodyPosX, monsterBodyPosY);

    duelObj.level = level;
    duelObj.renderObj = renderObj; //создаём объект, в который помещаем все необходимые данные
    duelObj.player = player;
    duelObj.monster = monster;

    document.querySelector('.monster-hp').style.width = `${monster.health}%`;
    document.querySelector('.monster-hp').textContent = `${monster.health}`;

    document.querySelector('.player-hp').style.width = `${player.health}%`;
    document.querySelector('.player-hp').textContent = `${player.health}`;

    levelTitle();
    battle(duelObj);
}

function clearBattlefield(renderObj) {
    delete renderObj.tower;
    delete renderObj.monsterWeapon;
    delete renderObj.monsterBody;
    delete renderObj.monsterHead;
};

function changeHealth(duelObj, obj, value) {
    if (obj instanceof Monster) {
        obj.health += value;
        let healthBar = document.querySelector('.monster-hp');
        healthBar.textContent = obj.health;
        healthBar.style.width = `${obj.health}%`;
        monsterAttack(duelObj); // если это не отрисовка, после удара по монстру идёт ответный ход
    } else {
        obj.health += value;
        let healthBar = document.querySelector('.player-hp');
        healthBar.textContent = obj.health;
        healthBar.style.width = `${obj.health}%`;
        battle(duelObj); //если это не отрисовка, то после удара по игроку идёт следующий раунд

    }
}

function battle(duelObj) {
    if (duelObj.player.health > 0 && duelObj.monster.health > 0) {
        selectSpell(duelObj);
    } else if (duelObj.monster.health <= 0) {

        duelObj.level++; //повышаем урон наносимый монстром и уровень
        levelTitle();
        duelStart(duelObj.level, duelObj.renderObj, duelObj.player); //"бесконечный" цикл игры

    } else if (duelObj.player.health <= 0) {
        alert(`Your score is ${duelObj.player.score}`);
        let score = duelObj.player.score;
        let name = duelObj.player.name;
        let obj = {};
        obj[name] = score;
        updateLeaderboard(obj);
    }
};

function selectSpell(duelObj) {
    let main = document.getElementById('main');

    let modal = require("html-loader!../../components/modal-dialog/modal.html");
    canvas.insertAdjacentHTML('afterend', modal);
    /*main.innerHTML = '<canvas id="canvas"></canvas>' + modal;*/
    modal = document.getElementById('modal-dialog');
    let description = document.querySelector('.description');
    description.innerHTML = '<h2>Select your spell by taping number or using tab/mouse</h2>';
    description.style.display = 'block';
    document.getElementsByClassName('spellchoise')[0].style.display = 'block';
    modal.addEventListener('keypress', (e) => {
        if (e.key == '1') {
            e.preventDefault();
            selectBolt('firebolt');
        }
        if (e.key == '2') {
            e.preventDefault();
            selectBolt('icebolt');
        }
        if (e.key == '3') {
            e.preventDefault();
            selectBolt('thunderbolt');
        }
    });

    main.insertBefore(modal, main.children[0]);

    document.querySelector('.select-firebolt').focus();

    function selectBolt(bolt) {
        duelObj.player.currentSpell = bolt;
        modal.classList.add('hide');
        main.removeChild(modal);
        runRandomTask(duelObj);
    };
    document.querySelector('.select-firebolt').addEventListener('click', () => {
        selectBolt('firebolt');
    });
    document.querySelector('.select-iceball').addEventListener('click', () => {
        selectBolt('icebolt');
    });
    document.querySelector('.select-thunderbolt').addEventListener('click', () => {
        selectBolt('thunderbolt');
    });
};

function playerAttack(result) {
    if (result === true) {
        duelObj.player.score += duelObj.level * 10;

        renderObj['character'] = new Sprite(['character', 'attack'], './image/Cast_127х69.png', [0, 0], [127, 69], 1, 10, 7, [10, (canvas.height - 68)], [0, 0], true);

        let bolt;
        //(type, url, pos, size, changeCoeff, animationSpeed, frames, startPosOnCanv, speed, once)
        if (duelObj.player.currentSpell == 'thunderbolt') {
            bolt = new Sprite([`${duelObj.player.currentSpell}`], `./image/${duelObj.player.currentSpell}.png`, [0, 0], [64, 64], 1, 10, 3, [duelObj.player.bodyPos[0] + 50, duelObj.player.bodyPos[1] + 10], [0.3, 0], false);

        } else {
            bolt = new Sprite([`${duelObj.player.currentSpell}`], `./image/${duelObj.player.currentSpell}.png`, [0, 0], [32, 32], 1, 10, 3, [duelObj.player.bodyPos[0] + 50, duelObj.player.bodyPos[1] + 10], [0.3, 0], false);
        }
        let sound = new Audio(`./sounds/${duelObj.player.currentSpell}.wav`);
        sound.play();
        bolt.hit = changeHealth.bind(this, duelObj, duelObj.monster, -(duelObj.monster.damage + 10)); //ждём когда заклинание достигнет цели
        duelObj.renderObj.bolt = bolt; //рисуем болт

    } else {
        monsterAttack(duelObj); //монстр ждёт любого результата, потом атакует
    }
};

function runRandomTask(duelObj) {
    let tasksArr = [listeningTask, translateTask, arithmeticTask, notationTask, historyTask, triangleTask, colorTask, animalsTask, idiomTask, dragndropTask];

    tasksArr[Math.floor(Math.random() * (tasksArr.length))].call(this, playerAttack); //вызов по нажатии
}
// отдельная ф-я  для анимации
function monsterAttack(duelObj) {
    let sound = new Audio('./sounds/monster.ogg');
    sound.play();
    duelObj.renderObj.monsterWeapon.hit = changeHealth.bind(this, duelObj, duelObj.player, -(duelObj.monster.damage));
    duelObj.renderObj.monsterWeapon.speed = [-0.3, 0];
}
