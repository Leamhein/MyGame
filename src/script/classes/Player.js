import {
    resCache
} from '../engine/resources';

export class Player {
    constructor(health, bodyPos, damage, name, score) {
        this.health = health;
        this.bodyPos = bodyPos;
        this.name = name;
        this.damage = damage;
        this.score = score;
	}
}