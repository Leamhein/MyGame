import {
    resCache,
    resources
} from '../engine/resources';

import {
    character,
    renderObj
} from '../../screens/home/index';

export class Sprite {
    constructor(type, url, pos, size, changeCoeff, animationSpeed, frames, startPosOnCanv, speed, once) {
        /*оформление кэша ресурсов в стиле resCache['enemy']['head']*/
        let cachePos = resCache;
        if (Array.isArray(type)) { //можно создать многомерный объект
            type.forEach((item, i) => {
                if (i == type.length - 1) {
                    cachePos[item] = this;
                } else {
                    cachePos[item] = {};
                    cachePos = cachePos[item];
                }
            });
            cachePos = this;
        } else {
            if (typeof type === 'string') {
                cachePos[type] = this;
            } else {
                throw new TypeError('TypeError, only array or string may used');
            }
        }
        this.url = url; // адрес изображения
        this.pos = pos; // позиция кадра на спрайте
        this.size = size; // размер одного спрайта
        this.renderSize = size.map(i => i * changeCoeff); // фактический размер объекта при рендере
        this.animSpeed = animationSpeed; // скорость анимации (больше = медленнее)
        this.frames = frames; // количество кадров анимации
        this.curFrame = 1; // текущий кадр
        this.startPosOnCanv = startPosOnCanv; // начальное положение объекта на канвасе
        this.speed = speed; //скорость перемещения объекта
        this.once = once; // воспроизводить один раз или зацикленно
        this.done = false; // анимация выполнена
        this.frameCounter = 0; //счетчик пропущенных кадров (регулировка скорости анимации)
    }

    render(ctx) {
        if (this.done || this.once && this.curFrame >= this.frames) { // проверка выполнен весь цикл анимации, для анимаций выполняенмых один раз
            this.done = true;
            if (this.url === './image/Cast_127х69.png') {
                renderObj['character'] = character;
            }
            return;
        } else {
            if (this.curFrame >= this.frames) { // проверка для цикличных анимаций
                this.curFrame = 1;
            }
        }
        let x = this.pos[0];
        x += this.size[0] * this.curFrame; // смещение по х для каждого кадра
        let y = this.pos[1]; // * по у

        ctx.drawImage(resources.get(this.url), x, y, this.size[0], this.size[1], this.startPosOnCanv[0], this.startPosOnCanv[1], this.renderSize[0], this.renderSize[1]); // отрисовка
        // управление скоростью отрисовки отдельных кадров анимации
        if (this.frameCounter >= this.animSpeed) {
            // изображдение кадра сменяется после пропуска n кадров (n заданно в this.animSpeed)
            this.curFrame += 1;
            this.frameCounter = 0;
        }
        this.frameCounter++;
    }

}
