import {
    resCache
} from '../engine/resources';

import {
    Sprite
} from './sprite';

export class Monster {
    constructor(health, bodyPos, damage) {
        this.health = health;
        this.bodyPos = bodyPos;
        this.name = 'anonyumos';
        this.damage = damage;
    }
    generateName() {
        let json = resCache['JSON']['https://raw.githubusercontent.com/Leamhein/Json/master/monsterNames.json'];
        let firstName = Math.floor(Math.random() * json.firstName.length);
        let secondName = Math.floor(Math.random() * json.secondName.length);
        let thirdName = Math.floor(Math.random() * json.thirdName.length);

        let str = `${json.firstName[firstName]} ${json.secondName[secondName]} ${json.thirdName[thirdName]}`;

        this.name = str;
        return str;
    }
    generateHead(monsterBodyPosX, monsterBodyPosY) {
        let monsterHeads = [];
        monsterHeads.push(new Sprite(['monster', 'monsterHead-1'], './image/monster-head-1.png', [-50, 0], [50, 35], 1, 0, 1, [(monsterBodyPosX - 30), (monsterBodyPosY - 20)], [0, 0], false));
        monsterHeads.push(new Sprite(['monster', 'monsterHead-2'], './image/monster-head-2.png', [-36, 0], [36, 39], 1, 0, 1, [(monsterBodyPosX - 21), (monsterBodyPosY - 20)], [0, 0], false));
        monsterHeads.push(new Sprite(['monster', 'monsterHead-3'], './image/monster-head-3.png', [-34, 0], [34, 40], 1, 0, 1, [(monsterBodyPosX - 10), (monsterBodyPosY - 25)], [0, 0], false));
        monsterHeads.push(new Sprite(['monster', 'monsterHead-4'], './image/monster-head-4.png', [-36, 0], [36, 39], 1, 0, 1, [(monsterBodyPosX - 15), (monsterBodyPosY - 15)], [0, 0], false));

        return monsterHeads[Math.floor(Math.random() * (monsterHeads.length))];
    }
    generateWeapon(monsterBodyPosX, monsterBodyPosY) {
        let monsterWeapons = [];
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/sword.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/swordWood.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/axe.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/axe2.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/axeDouble.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/axeDouble2.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/hammer.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));
        monsterWeapons.push(new Sprite(['monster', 'monsterWeapon-1'], './image/wand.png', [-64, 0], [64, 64], 1, 0, 1, [(monsterBodyPosX - 40), (monsterBodyPosY)], [0, 0], false));

        return monsterWeapons[Math.floor(Math.random() * (monsterWeapons.length))];
    }
    generateBody(monsterBodyPosX, monsterBodyPosY) {
        let monsterBodies = []
        monsterBodies.push(new Sprite(['monster', 'monsterBodyBlue'], './image/monster-armor-blue.png', [-36, 0], [36, 100], 1, 0, 1, [monsterBodyPosX, monsterBodyPosY], [0, 0], false));
        monsterBodies.push(new Sprite(['monster', 'monsterBodyRed'], './image/monster-armor-red.png', [-36, 0], [36, 100], 1, 0, 1, [monsterBodyPosX, monsterBodyPosY], [0, 0], false));
        monsterBodies.push(new Sprite(['monster', 'monsterBodyYellow'], './image/monster-armor-yellow.png', [-36, 0], [36, 100], 1, 0, 1, [monsterBodyPosX, monsterBodyPosY], [0, 0], false));
        monsterBodies.push(new Sprite(['monster', 'monsterBodyGreen'], './image/monster-armor-green.png', [-36, 0], [36, 100], 1, 0, 1, [monsterBodyPosX, monsterBodyPosY], [0, 0], false));

        return monsterBodies[Math.floor(Math.random() * (monsterBodies.length))];
    }
}
