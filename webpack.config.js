const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry: './src/screens/home/index.js',
    output: {
        filename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
			},
            {
                test: /\.tpl\.html$/,
                loader: 'html-loader'
            },
            /*{
                test: /\.css$/,
                
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader'
                })
            }*/
		]
    },

    plugins: [
        new ExtractTextPlugin({
            filename: 'style.[chunkhash].css',
            disable: false,
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            inject: false,
            hash: true,
            template: './src/screens/home/index.html',
            filename: 'index.html'
        })
        /*,
                new CopyWebpackPlugin([{
                        from: './src/font',
                        to: './font'
              },
                    {
                        from: './src/image',
                        to: './image'
              }
            ])*/
    ],
    watch: false,
    watchOptions: {
        aggregateTimeout: 200
    },
    mode: 'development'
};
